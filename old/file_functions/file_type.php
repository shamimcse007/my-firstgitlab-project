
<!--filetype — Gets file type-->
<!--Returns the type of the file. Possible values are fifo, char, dir, block, link, file, socket and unknown.-->
<?php

echo filetype('/etc/passwd');  // file
echo filetype('/etc/');        // dir

?>