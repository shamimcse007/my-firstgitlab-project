<!--basename — Returns trailing name component of path-->
<!--basename() operates naively on the input string, and is not aware of the actual filesystem, or path components such as "..".-->

<?php
echo "1)".basename ("/etc/sudoers.d",".d").PHP_EOL ;
echo "2)".basename ("/etc/sudoers.d").PHP_EOL;
echo "3)".basename  ("/etc/passwd"). PHP_EOL;

?>