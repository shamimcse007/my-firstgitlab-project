<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6ab316dd5aa27bfb69ce02ef18a7614d
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/source',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6ab316dd5aa27bfb69ce02ef18a7614d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6ab316dd5aa27bfb69ce02ef18a7614d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
